Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://b326.gitlab.io/intrieri1998/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/intrieri1998/1.3.0/


.. image:: https://b326.gitlab.io/intrieri1998/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/intrieri1998


.. image:: https://b326.gitlab.io/intrieri1998/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/intrieri1998/


.. image:: https://badge.fury.io/py/intrieri1998.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/intrieri1998




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/intrieri1998/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/intrieri1998/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/intrieri1998/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/intrieri1998/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/intrieri1998/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/intrieri1998/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/intrieri1998/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/intrieri1998/commits/prod
.. #}

Data and formalisms from Intrieri et al. (1998)
