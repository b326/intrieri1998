"""
Figure 6
========

Plot measures from figure 6
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1998 import fig6, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig6.csv", sep=";", comment="#")

# regressions
ifracs = np.linspace(40, 60, 100)
reg = fig6.reg(ifracs)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]

for (mod, hour), sdf in meas.dropna().groupby(['mod', 'solar_time']):
    azim = mod.split("_")[1]
    ax.plot(sdf['ifrac'], sdf['tce'], 's', label=f"{azim} {hour:d}")

ax.plot(ifracs, reg, '-', color='#aaaaaa')

ax.legend(loc='upper left')
ax.set_ylabel("TCE [mmol H2O.plant-1.s-1]")
ax.set_ylim(3, 8)
ax.set_yticks(range(3, 9, 1))

ax.set_xlabel("Total light interception [%]")
ax.set_xlim(35, 65)
ax.set_xticks(range(35, 66, 10))

fig.tight_layout()
plt.show()
