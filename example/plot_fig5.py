"""
Figure 5
========

Plot measures from figure 5
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1998 import fig5, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#", index_col=[0])

# regressions
pfds = np.linspace(0, 1800, 100)
reg = dict(
    ns=fig5.ns_reg(pfds),
    ew=fig5.ew_reg(pfds)
)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]

for azim in ('ns', 'ew'):
    crv, = ax.plot(meas[f'pfd_{azim}'], meas[f'tce_{azim}'], '^', label=f"{azim}")
    ax.plot(pfds, reg[azim], '-', color=crv.get_color())

ax.set_ylabel("TCE [mmol H2O.plant-1.s-1]")
ax.set_ylim(0, 9)
ax.set_yticks(range(0, 10, 1))

ax.legend(loc='upper left', ncol=2)
ax.set_xlabel("PFD [µmol photon.m-2.s-1]")
ax.set_xlim(0, 1800)
ax.set_xticks(range(0, 1801, 300))

fig.tight_layout()
plt.show()
