"""
Figure 3
========

Plot measures from figure 3
"""
import matplotlib.pyplot as plt
import pandas as pd

from intrieri1998 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig3.csv", sep=";", comment="#", index_col=['solar_time'])

# plot result
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(10, 7), squeeze=False)
for j, azim in enumerate(['ns', 'ew']):
    ax = axes[0, j]
    ax.plot(meas.index, meas[f'pfd_{azim}'], '-')

    ax.set_ylabel("PFD [µmol photon.m-2.s-1]")
    ax.set_ylim(0, 1800)
    ax.set_yticks(range(0, 1801, 300))

    ax = ax.twinx()
    ax.plot(meas.index, meas[f'tca_{azim}'], 'o', label="tca")
    ax.plot(meas.index, meas[f'tce_{azim}'], '^', label="tce")

    ax.legend(loc='lower center', ncol=2)
    ax.set_ylabel("TCA [µmol CO2.plant-1.s-1], TCE [mmol H2O.plant-1.s-1]")
    ax.set_ylim(-4, 30)
    ax.set_yticks(range(-4, 29, 4))

    ax = axes[1, j]
    ax.plot(meas.index, meas[f'co2_{azim}'], '-')

    ax.set_ylabel("CO2 [µl.l-1]")
    ax.set_ylim(0, 500 if j == 0 else 600)
    ax.set_yticks(range(0, 501 if j == 0 else 601, 50))

    ax = ax.twinx()
    ax.plot(meas.index, meas[f'wue_{azim}'], 'v', label="wue")
    ax.plot(meas.index, meas[f'vpd_{azim}'], 's', label="vpd")

    ax.legend(loc='center right')
    ax.set_ylabel("WUE [µmol CO2.mmol H20-1], VPD [kPa]")
    ax.set_ylim(0, 12)
    ax.set_yticks(range(13))

for ax in axes[-1, :]:
    ax.set_xlabel("solar time [h]")
    ax.set_xlim(4, 22)
    ax.set_xticks(range(4, 23, 2))

fig.tight_layout()
plt.show()
