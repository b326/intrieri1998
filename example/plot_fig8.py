"""
Figure 8
========

Plot measures from figure 8
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1998 import fig8, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#")

# regressions
pfds = np.linspace(0, 1800, 100)
reg = [fig8.reg(x) for x in pfds]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]
ax.plot(meas['pfd'], meas['tce'], '^')
ax.plot(pfds, reg, '-', color='#aaaaaa')

ax.set_ylabel("TCE [mmol H2O.plant-1.s-1]")
ax.set_ylim(0, 20)
ax.set_yticks(range(0, 21, 2))

ax.set_xlabel("PFD [µmol photon.m-2.s-1]")
ax.set_xlim(0, 1800)
ax.set_xticks(range(0, 1801, 300))

fig.tight_layout()
plt.show()
