"""
Figure 7
========

Plot measures from figure 7
"""
import matplotlib.pyplot as plt
import pandas as pd

from intrieri1998 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig7.csv", sep=";", comment="#", index_col=['solar_time'])

# plot result
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(6, 7), squeeze=False)
ax = axes[0, 0]
ax.plot(meas.index, meas[f'pfd'], '-')

ax.set_ylabel("PFD [µmol photon.m-2.s-1]")
ax.set_ylim(0, 1800)
ax.set_yticks(range(0, 1801, 300))

ax = ax.twinx()
ax.plot(meas.index, meas['tca'], 'o', label="tca")
ax.plot(meas.index, meas['tce'], '^', label="tce")

ax.legend(loc='upper right')
ax.set_ylabel("TCA [µmol CO2.plant-1.s-1], TCE [mmol H2O.plant-1.s-1]")
ax.set_ylim(-2, 60)
ax.set_yticks(range(0, 61, 5))

ax = axes[1, 0]
ax.plot(meas.index, meas[f'co2'], '-')

ax.set_ylabel("CO2 [µl.l-1]")
ax.set_ylim(0, 500)
ax.set_yticks(range(0, 501, 50))

ax = ax.twinx()
ax.plot(meas.index, meas['vpd'], 's', label="vpd")
ax.plot(meas.index, meas['wue'], 'v', label="wue")

ax.legend(loc='upper right')
ax.set_ylabel("WUE [µmol CO2.mmol H20-1], VPD [kPa]")
ax.set_ylim(0, 10)
ax.set_yticks(range(0, 11, 1))

for ax in axes[-1, :]:
    ax.set_xlabel("solar time [h]")
    ax.set_xlim(4, 22)
    ax.set_xticks(range(4, 23, 2))

fig.tight_layout()
plt.show()
