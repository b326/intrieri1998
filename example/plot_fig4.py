"""
Figure 4
========

Plot measures from figure 4
"""
import matplotlib.pyplot as plt
import pandas as pd

from intrieri1998 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#", index_col=['solar_time'])

# plot result
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(6, 7), squeeze=False)
ax = axes[0, 0]
ax.plot(meas.index, meas[f'pfd'], '-')

ax.set_ylabel("PFD [µmol photon.m-2.s-1]")
ax.set_ylim(0, 1800)
ax.set_yticks(range(0, 1801, 300))

ax = ax.twinx()
for azim in ('ns', 'ew'):
    ax.plot(meas.index, meas[f'tca_{azim}'], 'o', label=f"tca_{azim}")
    ax.plot(meas.index, meas[f'tce_{azim}'], '^', label=f"tce_{azim}")

ax.legend(loc='upper center', ncol=4)
ax.set_ylabel("TCA [µmol CO2.plant-1.s-1], TCE [mmol H2O.plant-1.s-1]")
ax.set_ylim(-2, 30)
ax.set_yticks(range(0, 29, 4))

ax = axes[1, 0]
ax.plot(meas.index, meas[f'co2'], '-')

ax.set_ylabel("CO2 [µl.l-1]")
ax.set_ylim(0, 500)
ax.set_yticks(range(0, 501, 50))

ax = ax.twinx()
ax.plot(meas.index, meas[f'vpd'], 's', label=f"vpd")
for azim in ('ns', 'ew'):
    ax.plot(meas.index, meas[f'wue_{azim}'], 'v', label=f"wue_{azim}")

ax.legend(loc='upper center', ncol=3)
ax.set_ylabel("WUE [µmol CO2.mmol H20-1], VPD [kPa]")
ax.set_ylim(0, 20)
ax.set_yticks(range(0, 21, 5))

for ax in axes[-1, :]:
    ax.set_xlabel("solar time [h]")
    ax.set_xlim(4, 22)
    ax.set_xticks(range(4, 23, 2))

fig.tight_layout()
plt.show()
