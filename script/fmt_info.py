"""
General information from text
"""
import json
from pathlib import Path

from intrieri1998 import pth_clean

# read raw
info = json.load(open("../raw/info.json"))


# format
def rm_comment_tag(data):
    for key, val in tuple(data.items()):
        if key.endswith("_comment"):
            del data[key]
        elif isinstance(val, dict):
            rm_comment_tag(val)


rm_comment_tag(info)

# write result
info["_comment"] = f"This file has been formatted by {Path(__file__).name}"
json.dump(info, open(pth_clean / "info.json", 'w'), sort_keys=True, indent=2)
